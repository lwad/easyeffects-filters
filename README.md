# Easyeffects Filters

The filters for all my audio devices, aiming for as close to a neutral sound as possible.

| Name      | Device          |
| --------- | --------------- |
| AKG       | AKG K271 Mk.II  |
| KZ        | KZ ZSN Pro X    |
| Logi Z407 | Logitech Z407   |
| Sony      | Sony WH1000-XM5 |
